<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderServiceTest extends WebTestCase
{
    public function testSubmitOrder()
    {
        $client = static::createClient();

        $client->request('POST', '/orders',
            array(
                "title"=>"Need to paint my garage",
                "zipcode"=>"01623",
                "description"=>"Garage paint has completely gone",
                "execution_date"=>"2018-08-27",
                "category_id"=>"402020"
            ));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }     
}