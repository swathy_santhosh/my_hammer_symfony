<?php

namespace App\tests\Repository;

use App\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LocationRepositoryTest extends KernelTestCase
{

    /**
    * @var \Doctrine\ORM\EntityManager
    **/

    private $entityManager;

    /**
    * {@inheritDoc}
    **/

    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();


    }


    public function testFindByZipCode()
    {
        $zipcodes = $this->entityManager->getRepository(Location::class)->findByZipCode("10");

        $this->assertCount(1, $zipcodes);
             

    }


    public function testGetZipCodes()
    {
        $zipcodes = $this->entityManager->getRepository(Location::class)->getZipCodes();

        $this->assertCount(6,$zipcodes);

    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }

}