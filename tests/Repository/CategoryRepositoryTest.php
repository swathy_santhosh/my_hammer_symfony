<?php

namespace App\tests\Repository;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryRepositoryTest extends KernelTestCase
{

    /**
    * @var \Doctrine\ORM\EntityManager
    **/

    private $entityManager;

    /**
    * {@inheritDoc}
    **/

    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();


    }


    public function testListAll()
    {
        $categories = $this->entityManager->getRepository(Category::class)->ListAll();

        $this->assertCount(5, $categories);

    }


    public function testGetArrayValues()
    {
        $categories = $this->entityManager->getRepository(Category::class)->GetArrayValues();

        $this->assertCount(5,$categories);

    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }

}