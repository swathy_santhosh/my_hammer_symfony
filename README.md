This application is developed using PHP 7.1 with Symfony 4.1 framework. It uses MYSQL as database
  
Kindly follow the below steps to run this application.

1 Clone this application using the below command in your local machine.
    git clone https://swathy_santhosh@bitbucket.org/swathy_santhosh/my_hammer_symfony.git
     

2 Navigate to the directory in terminal where this source code is cloned. 

3 Run "composer install" .This will install the dependencies that is required for the application to run.

4 Open the source folder in any IDE (such as Visual studio code,sublime etc) and create .env (copy           .env.dist)

5 Configure .env file by providing the required value such as db credentials.Before testing database    connectivity, create database for this application (eg. swathy_hammer_prj_symfony) 

6 To run database migrations :  
        php bin/console doctrine:migrations:migrate

        check if the tables are created in the database (category, orders,location)
7 To dataseed the table :
        php bin/console doctrine:fixtures:load 

        check if the category and location tables are loaded with initial data that is required to demonstrate this coding challenge.

8 To run the application :
      php bin/console server:run
    in some cases, try running if the above command does nt work.
      php bin/console server:start 

9 The list of routes are available in the below url
    https://documenter.getpostman.com/view/1840561/RWTsrFTM

10  To run test :
      ./bin/phpunit

         


        


