<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

// Function to get all the categories list
    public function ListAll(){

       $categories = $this->createQueryBuilder('c')->select("c.id","c.category_name","c.category_img")
                ->orderBy('c.id', 'ASC')                
                ->getQuery()
                ->getResult();
        
        
        return $categories;
 

    }

    //Function to populate array values 
    public function getArrayValues(){
        $cat_array = $this->ListAll();

         $categories = array();
         for($i=0;$i<count($cat_array);$i++){            
            $categories[$i]= $cat_array[$i]['id'];
             
         }
        return $categories;
    } 
 
}
