<?php

namespace App\Repository;

use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository 
{
      
    protected $entityManager;
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Location::class);
         
    }


     
//    /**
//     * @return Location[] Returns an array of Location objects
//     *    Function to retrieve list of location to be displayed in the dropdown.
//     */
 
    public function findByZipCode($value)
    {
        $cities = $this->createQueryBuilder('l')->select("l.zipcode","l.city")
            ->andWhere('l.zipcode LIKE :val and l.country = :cntry')
            ->setParameter('val', '%'.$value.'%')
            ->setParameter('cntry', "Germany")
            ->orderBy('l.id', 'ASC')          
            ->getQuery()
            ->getResult();
        
        
        return $cities;

    
    }


    
// Function to get zipcode for german country -- Filter by country ="germany"
     public function getZipCodes(){
         $zipcodes =  $this->createQueryBuilder('l')->select("l.zipcode") 
                    ->where('l.country = :val')
                    ->setParameter('val', "Germany")
                    ->getQuery()->getResult();
         
               

        $location = array();
         for($i=0;$i<count($zipcodes);$i++){            
            $location[$i]= $zipcodes[$i]['zipcode'];
             
         }
        
      return $location;

     }
 
 
   
 

     
}
