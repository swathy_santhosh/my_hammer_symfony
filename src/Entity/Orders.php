<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

use App\Validator as CustomAssert;

  

 

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * 
 */
class Orders
{

    
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *  @ORM\Column(type="integer", length=6)
     *  @CustomAssert\CategoryConstraint
     */
    private $category_id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(min=5,max=50,minMessage = "Your title must be at least 5 characters long",
     * maxMessage = "Your title cannot be longer than 50 characters")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=5)
     * @CustomAssert\ZipCodeConstraint
     */
    private $zipcode;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * 
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTime")
     */
    private $execution_date;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTime")
     * @Assert\NotBlank()
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTime")
     * @Assert\NotBlank()
     */
    private $updated_at;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $user_id;
      /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
      private $status;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryId(): ?int
    {
        return $this->category_id;
    }

    public function setCategoryId(?int $category_id): self
    {
        
        $this->category_id = $category_id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getExecutionDate(): ?\DateTimeInterface
    {
        return $this->execution_date;
    }

    public function setExecutionDate(\DateTimeInterface $execution_date): self
    {
        $this->execution_date = $execution_date;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

     

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->created_at = new \DateTime("now");
        return $this;
    }
    

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function setUpdatedAt()
    {
       return $this->updated_at= new \DateTime("now");
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): ?self
    {
        $this->status = $status;

        return $this;
    }

     

}
