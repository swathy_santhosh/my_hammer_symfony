<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * 
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300)
     * @Assert\NotBlank()
     * @Assert\Length(max=300)
     */
    private $category_name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTime")
     */
    private $updated_at;

    /**
    * @ORM\Column(type ="string")
    * @Assert\NotBlank()
    *  
    */
    private $category_img;

    public function getId(): ?int
    {
        return $this->id;
    }

     public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
    public function getCategoryName(): ?string
    {
        return $this->category_name;
    }

    public function setCategoryName(string $category_name): self
    {
        $this->category_name = $category_name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

 

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

     /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
      public function setCreatedAt()
    {
       return $this->created_at = new \DateTime("now");
         
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function setUpdatedAt()
    {
       return $this->updated_at = new \DateTime("now");
    }

    public function getCategoryImg(): ?string
    {
        return $this->category_img;
    }

    public function setCategoryImg(string $category_img): self
    {
        $this->category_img = $category_img;

        return $this;
    }

}
