<?php

namespace App\DataFixtures;


use App\Entity\Location;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

//class AppFixtures extends Fixture
class LocationFixture extends Fixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

        // For Seeding data in to table

       $input = array(
           0=>array(
                "zipcode"=> "10115",
                "city"   => "Berlin",
                "country"=> "Germany"
            ),
           1=>array(
                "zipcode"=> "10112",
                "city"   => "Porta Westfalica",
                "country"=> "Germany"
            ),
           2=>array(
                "zipcode"=> "01623",
                "city"   => "Lommatzsch",
                "country"=> "Germany"
           ),
           3=>array(
                "zipcode"=> "21521",
                "city"   => "Hamburg",
                "country"=> "Germany"
            ),
           4=>array(
                "zipcode"=> "06895",
                "city"   => "Bülzig",
                "country"=> "Germany"
            ),
           5=>array(
                "zipcode"=> "01612",
                "city"   => "Diesbar-Seußlitz",
                "country"=> "Germany"
           )
          );


        // create 20 products! Bam!
        for ($i = 0; $i < count($input); $i++) {
            $location = new Location();
            $location->setZipcode($input[$i]["zipcode"]);
            $location->setCity($input[$i]["city"]);
            $location->setCountry($input[$i]["country"]);
            $location->setCreatedAt();
            $location->setUpdatedAt();
            $manager->persist($location);
        }

        $manager->flush();
     }
}