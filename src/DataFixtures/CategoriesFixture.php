<?php

namespace App\DataFixtures;


use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

//class AppFixtures extends Fixture
class CategoriesFixture extends Fixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

       $input = array(
           0=>array(
                    'id'=>804040,
                    'category_name'=>"Sonstige Umzugsleistungen",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_25.svg?v=1535369703"
                   
                ),
                1=>array(
                    'id'=>802030,
                    'category_name'=>"Abtransport, Entsorgung und Entrümpelung",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_29.svg?v=1535369703"

                ),

                2=>array(
                    'id'=>411070,
                    'category_name'=>"Fensterreinigung",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_28.svg?v=1535369703"

                ),
                3=>array(
                    'id'=>402020,
                    'category_name'=>"Holzdielen schleifen",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_11.svg?v=1535369703"

                ),
                4=>array(
                    'id'=>108140,
                    'category_name'=>"Kellersanierung",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_22.svg?v=1535369703"

                )
          );


        // create 5 categories! Bam!
        for ($i = 0; $i < count($input); $i++) {
            $categories = new Category();
            $categories->setId($input[$i]["id"]);
            $categories->setCategoryName($input[$i]["category_name"]);   
            $categories->setCategoryImg($input[$i]["category_img"]);      
            $categories->setCreatedAt();
            $categories->setUpdatedAt();
            $manager->persist($categories);
        }

        $manager->flush();
     }
}