<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Category;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="list_categories")
     */
    public function index()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->ListAll();
        return new JsonResponse(["data"=>$categories]);  
    }
}
