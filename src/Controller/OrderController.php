<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
 
use Symfony\Component\HttpFoundation\Request;

use App\Service\OrderService;
 

class OrderController extends AbstractController
{   

    protected $orders;

    protected $entityManager;

    public function __construct(OrderService $orders){
        $this->orders = $orders;
       
        
    }



    /**
     * @Route("/orders", name="order_submit")
     */
    public function store(Request $request)
    {
         $entityManager = $this->getDoctrine()->getManager();         
         return $this->orders->submit_order($request,$entityManager);        
    }
}
