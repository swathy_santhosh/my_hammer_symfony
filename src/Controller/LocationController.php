<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Location;
 

class LocationController extends AbstractController
{

    //  

    /**
     * @Route("/list/locations/{query_string}", name="location_show")
     */
    public function show($query_string)
    {
         $cities = $this->getDoctrine()->getRepository(Location::class)->findByZipCode($query_string); 
        if(count($cities)>0){
            return new JsonResponse(["data"=>$cities]);
        }else{
            return new JsonResponse(["data"=>[]]);
        }
    

    }

 
}
