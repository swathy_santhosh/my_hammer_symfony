<?php

namespace App\Validator;

 use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Repository\CategoryRepository;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

use App\Entity\Category;

use Symfony\Component\Validator\Exception\UnexpectedTypeException;  

 /** @Annotation */
class CategoryConstraintValidator extends ConstraintValidator
{
    protected $cat_rep;
    protected $order;

    public function __construct(CategoryRepository $cat_rep){
        $this->cat_rep = $cat_rep;
         
    }

    public function validatedBy()
    {
        return get_class($this).'Validator';
        
    }
 


    public function validate($category, Constraint $constraint)
    {

        if (null === $category || '' === $category) {
            return;
        }
        
        if (!is_integer($category)) {
            throw new UnexpectedTypeException($category, 'integer');
        } 

        $categories = $this->cat_rep->getArrayValues();
       
         if (!in_array($category, $categories)) {
                $this->context->buildViolation("The given category id does not exists in database")
                    ->setParameter('{{ string }}', $category)
                    ->addViolation();
            }
       
    }

     
}