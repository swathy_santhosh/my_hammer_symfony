<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *  
 */
class ZipCodeConstraint extends Constraint
{
    public $message = 'The zipcode "{{ string }}" does not belongs to Germany';
}