<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *  
 */
class CategoryConstraint extends Constraint
{
    public $message = 'Invalid Category Id';
}