<?php

namespace App\Validator;

 use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Repository\LocationRepository;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

use App\Entity\Orders;

use Symfony\Component\Validator\Exception\UnexpectedTypeException;  

 /** @Annotation */
class ZipCodeConstraintValidator extends ConstraintValidator
{
    protected $loc_rep;
    protected $order;

    public function __construct(LocationRepository $loc_rep){
        $this->loc_rep = $loc_rep;
         
    }

    public function validatedBy()
    {
        return get_class($this).'Validator';
        
    }
 


    public function validate($order, Constraint $constraint)
    {

        if (null === $order || '' === $order) {
            return;
        }
        
        if (!is_string($order)) {
            throw new UnexpectedTypeException($order, 'string');
        } 

        $zipcodes = $this->loc_rep->getZipCodes();
       
         if (!in_array($order, $zipcodes)) {
                $this->context->buildViolation("Please provide valid german zipcode")
                    ->setParameter('{{ string }}', $order)
                    ->addViolation();
            }
       
    }

     
}