<?php

namespace App\Service;
 
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Orders;
use DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Response;

class OrderService 
{
     

    public function __construct(ValidatorInterface $validator){
       $this->validator =$validator;
    }
     


    public function submit_order($data,$em){ 

        $request = $data->request;
        $order = new Orders(); 
        $order->setCategoryId($request->get("category_id"));
        $order->setZipcode($request->get("zipcode"));
        $order->setDescription($request->get("description"));                    
        $order->setExecutionDate(\DateTime::createFromFormat('Y-m-d',$request->get("execution_date")));
        $order->setTitle($request->get("title"));
        // in real time the api gets value from client
        //$order->setUserId($request->get("user_id"));
        $order->setUserId(1); //hard coded for demonstration purpose
        $order->setStatus(0); // 0->means in progress, once the job is completed, we can update the value as 1 in update method.
        $order->setCreatedAt();  
        $order->setUpdatedAt();


        /* Validating errors on Order object */
        $errors = $this->validator->validate($order);

         if (count($errors) > 0) {
            
             $errorsString = (string) $errors;
            
            return new Response($errors);
        }

        // Persisting to table if no errors
        $em->persist($order);
         
        $em->flush();
        

        return new JsonResponse(array("data"=>"Succesfully submitted your order"));


    }






}